from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from faker import Faker

class Utilities:
    
    
    # global driver
    # def __init__(self):
    # fake = 

    def click_element_if_visible(self, driver, locator_type, locator):
        try:
            # Define the wait object with a timeout
            wait = WebDriverWait(driver, 15)
            # Wait until the element is visible
            element = wait.until(EC.visibility_of_element_located((locator_type, locator)))
            # Click the element
            element.click()
            print("Element clicked")
        except TimeoutException:
            # If the timeout is reached and the element is not visible, print a message
            print("Element not displayed")

    def click_multiple_elements(self, driver, locator_type, locator):
        try:
            # Find all elements that match the locator
            elements = driver.find_elements(locator_type, locator)
            # Click each element
            for element in elements:
                element.click()
            print(f"{len(elements)} elements clicked")
        except Exception as e:
            # Handle any exceptions that occur
            print(f"An error occurred: {e}")

    def assert_element_text(self, driver, locator_type, locator, expected_text):
        try:
            element = driver.find_element(locator_type, locator)
            actual_text = element.text
            assert actual_text == expected_text, f"Expected text '{expected_text}' but got '{actual_text}'"
            print("Assertion passed")
        except (AssertionError, NoSuchElementException) as e:
            print(f"Assertion failed: {e}")

    def generate_random_first_name():
        return Faker().first_name()

    def generate_random_last_name():
        return Faker().last_name()

    def generate_random_username():
        return Faker().user_name()
    
    def click_next_button(self,locator_type,locator):
        next_button = self.driver.find_element(locator_type, locator)
        next_button.click()
    
    def get_signup_form_errors(self):
        errors_signupForm = ["Please enter a valid name","The username must have at least 8 characters with a combination of alphabets & numbers. Only '-' and '_' are allowed as special characters.","Please enter a valid email id","Please enter a valid mobile number","The password must have at least 8 characters with a combination of alphabets, special characters & numbers.","Confirmation password not matching","Please accept the terms & condition."]
        return errors_signupForm
    
    def generate_error_locators(self, errors_signupForm):
        error_locators = []
        for error_text in errors_signupForm:
            locator = f"//android.widget.TextView[@text='{error_text}']"
            error_locators.append(locator)
        return error_locators
    
    # def input_fieldLocators():
        

    def assert_error_sequence(self, driver, next_button_locator_type, next_button_locator, error_locator_type, input_locator_type, input_locator):
        errors = self.get_signup_form_errors()
        error_locators = self.generate_error_locators(errors)
        for i in range(len(errors)):
            # print(error_locators[i])
        # for i in range(0,n): 
            self.click_next_button(driver, next_button_locator_type, next_button_locator)
            self.assert_element_text(driver, error_locator_type, error_locators[i], errors[i])
            input_field = self.driver.find_element(input_locator_type, input_locator)
            input_field.send_keys(self.generate_random_first_name())
    
