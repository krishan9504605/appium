from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
import time
from appium.webdriver.appium_service import AppiumService
from utilities import Utilities



class BaseClass:

    # global driver
    def __init__(self):
        self.locator = None
        self.driver = None
        self.utilities = Utilities()
        self.start_appium()
        

    def start_appium(self):
        # Start the Appium server
        self.service = AppiumService()
        self.service.start()
        print(self.service.is_running)
        options = UiAutomator2Options().load_capabilities(self.get_des_cap())
        self.driver = webdriver.Remote('http://127.0.0.1:4723', options=options)
        self.driver.implicitly_wait(10) 
        # self.appium_service.start()
    
    def stop_appium(self):
        # Stop the Appium server
        if self.service:
            self.service.stop()


    def get_des_cap(self):
        des_cap = {
        "app": "/Users/Admin/Documents/Appium/Apps/Unstop_9.4.2.apk",
        "automationName" : "UIAutomator2",
        "platformName" : "Android"
        }
        return des_cap

    def prompts(self):
        self.locator = self.driver.find_element 
        notificationPrompt = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.ID,"com.android.permissioncontroller:id/grant_dialog")))
        if notificationPrompt.is_displayed:
            self.locator(By.ID,"com.android.permissioncontroller:id/permission_allow_button").click()
        else:
            print('No prompt appearing')
            pass

    def signup(self):
        time.sleep(5)
        # self.locator(AppiumBy.XPATH,"//android.widget.TextView[@text='Login']").click()
        self.locator(By.XPATH,"//android.webkit.WebView").click()
        self.locator(By.XPATH,"//android.widget.TextView[@text='Login']").click()
         # Call the function with the desired element's locator
        self.utilities.click_element_if_visible(self.driver,By.XPATH, "//android.widget.Button[@text='Ok, Continue']")
        self.locator(By.XPATH,"//android.widget.TextView[@resource-id='sign-up']").click()
        #Click on all Candidates type:
        self.utilities.click_multiple_elements(self.driver,By.XPATH, "//android.view.View[@resource-id='s_menu']/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.widget.Image")
        # for i in range(0,7):     
        # self.utilities.click_next_button(By.XPATH,"//android.widget.Button[@text='Next']")
        # # self.locator(By.XPATH,"//android.widget.Button[@text='Next']").click()
        # self.utilities.assert_element_text(self.driver, By.XPATH,"//android.widget.TextView[@text='Please enter a valid name']", "Please enter a valid name")
        # self.locator(By.XPATH,"//android.widget.EditText[@resource-id='first_name']").send_keys(self.utilities.generate_random_first_name())
        self.utilities.assert_error_sequence(self.driver,7,By.XPATH,"//android.widget.Button[@text='Next']",)
        
        self.service.stop()
        
        

    